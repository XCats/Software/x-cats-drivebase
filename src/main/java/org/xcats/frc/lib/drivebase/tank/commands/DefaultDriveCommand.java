package org.xcats.frc.lib.drivebase.tank.commands;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.button.CommandJoystick;
import org.xcats.frc.lib.drivebase.tank.DriveBaseSubsystem;

public class DefaultDriveCommand extends CommandBase {

    DriveBaseSubsystem driveBaseSubsystem;
    CommandJoystick driverLeftStick;
    CommandJoystick driverRightStick;
    int joystickAxis;
    double speedMultiplier;

    public DefaultDriveCommand(DriveBaseSubsystem driveBaseSubsystem, CommandJoystick driverLeftStick, CommandJoystick driverRightStick, int joystickAxis)
    {
        this(driveBaseSubsystem, driverLeftStick, driverRightStick, joystickAxis, 1);
    }

    public DefaultDriveCommand(DriveBaseSubsystem driveBaseSubsystem, CommandJoystick driverLeftStick, CommandJoystick driverRightStick, int joystickAxis, double speedMultiplier)
    {
        this.driveBaseSubsystem = driveBaseSubsystem;
        this.driverLeftStick = driverLeftStick;
        this.driverRightStick = driverRightStick;
        this.joystickAxis = joystickAxis;
        this.speedMultiplier = speedMultiplier;

        addRequirements(driveBaseSubsystem);
    }

    @Override
    public void initialize()
    {
        
    }

    @Override
    public void execute()
    {
            this.driveBaseSubsystem.setLeft(MathUtil.applyDeadband(-driverLeftStick.getRawAxis(joystickAxis), 0.1) * speedMultiplier);
            this.driveBaseSubsystem.setRight(MathUtil.applyDeadband(-driverRightStick.getRawAxis(joystickAxis), 0.1) * speedMultiplier);
    }

    @Override
    public void end(boolean interupted)
    {

    }

    @Override
    public boolean isFinished()
    {
        return false;
    }
}
