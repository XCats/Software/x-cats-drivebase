package org.xcats.frc.lib.drivebase.tank.kinematics;

import com.ctre.phoenix.sensors.WPI_PigeonIMU;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.math.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.math.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.wpilibj.interfaces.Gyro;
import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import org.xcats.frc.lib.devices.sensors.XEncoder;
import org.xcats.frc.lib.devices.sensors.XGyro;
import org.xcats.frc.lib.drivebase.tank.DriveBaseSubsystem;

public class KinematicsDriveBaseSubsystem extends DriveBaseSubsystem {
    private Gyro gyro;
    private final DifferentialDriveOdometry odometry;
    public KinematicsDriveBaseSubsystem(MotorController driveLeft, MotorController driveRight, XEncoder leftEncoder, XEncoder rightEncoder, XGyro gyro) {
        super(driveLeft, driveRight, leftEncoder, rightEncoder);
        this.gyro = (WPI_PigeonIMU) gyro.getBaseObject();
        this.odometry = new DifferentialDriveOdometry(this.gyro.getRotation2d(), leftEncoder.getLinearDistance(), rightEncoder.getLinearDistance());
    }

    @Override
    public void periodic() {
        super.periodic();
        odometry.update(this.gyro.getRotation2d(), this.getLeftDistance(), this.getRightDistance());
    }

    public Pose2d getPose() {
        return odometry.getPoseMeters();
    }

    public DifferentialDriveWheelSpeeds getWheelSpeeds() {
        return new DifferentialDriveWheelSpeeds(this.getLeftRate(), this.getRightRate());
    }

    public void resetOdometry(Pose2d pose) {
        zeroEncoders();
        this.odometry.resetPosition(gyro.getRotation2d(), getLeftDistance(), getRightDistance(), pose);
    }

    public void tankDriveVolts(double leftVolts, double rightVolts) {
        getDriveLeft().setVoltage(leftVolts);
        getDriveRight().setVoltage(rightVolts);
    }
}
