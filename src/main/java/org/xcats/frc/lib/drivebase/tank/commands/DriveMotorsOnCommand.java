package org.xcats.frc.lib.drivebase.tank.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import org.xcats.frc.lib.drivebase.tank.DriveBaseSubsystem;

public class DriveMotorsOnCommand extends CommandBase {

    DriveBaseSubsystem driveBaseSubsystem;
    boolean isForward;
    double speed;

    public DriveMotorsOnCommand(DriveBaseSubsystem driveBaseSubsystem, boolean isForward)
    {
        this(driveBaseSubsystem, isForward, .5);
    }

    public DriveMotorsOnCommand(DriveBaseSubsystem driveBaseSubsystem, boolean isForward, double speed)
    {
        this.driveBaseSubsystem = driveBaseSubsystem;
        this.isForward = isForward;
        this.speed = speed;

        this.addRequirements(driveBaseSubsystem);
    }

    @Override
    public void initialize()
    {

    }

    @Override
    public void execute()
    {
        if(this.isForward)
        {
            this.driveBaseSubsystem.setLeft(speed);
            this.driveBaseSubsystem.setRight(speed);
        }
        else
        {
            this.driveBaseSubsystem.setLeft(-speed);
            this.driveBaseSubsystem.setRight(-speed);  
        }
    }

    @Override
    public void end(boolean interrupted)
    {

    }

    @Override
    public boolean isFinished()
    {
        return true;
    }
}
