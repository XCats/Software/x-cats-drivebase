package org.xcats.frc.lib.drivebase.tank;

import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import org.xcats.frc.lib.XSubsystem;
import org.xcats.frc.lib.devices.sensors.XEncoder;
import org.xcats.frc.lib.devices.sensors.impl.XEncoderGroup;

//Subsystem controlling the driving, also estimates robot position
public class DriveBaseSubsystem extends XSubsystem {
    
    private MotorController driveLeft;
    private MotorController driveRight;
    private XEncoder leftEncoder;
    private XEncoder rightEncoder;
    private XEncoder driveEncoder;

    public DriveBaseSubsystem(MotorController driveLeft, MotorController driveRight, XEncoder leftEncoder, XEncoder rightEncoder)
    {
        this.driveLeft = driveLeft;
        this.driveRight = driveRight;

        this.leftEncoder = leftEncoder;
        this.rightEncoder = rightEncoder;
        this.driveEncoder = new XEncoderGroup("driveEncoder", leftEncoder, rightEncoder);
    }

    public void setLeft(double speed) //sets left wheel speed
    {
        this.driveLeft.set(speed);
    }
    
    public void setRight(double speed) //sets right wheel speed
    {
        this.driveRight.set(speed);
    }

    public double getAverageDistance()
    {
        return driveEncoder.getLinearDistance();
    }

    public double getLeftDistance()
    {
        return leftEncoder.getLinearDistance();
    }

    public double getRightDistance()
    {
        return rightEncoder.getLinearDistance();
    }

    protected MotorController getDriveLeft() {
        return driveLeft;
    }

    protected MotorController getDriveRight() {
        return driveRight;
    }

    public double getLeftRate() {
        return leftEncoder.getLinearVelocity();
    }

    public double getRightRate() {
        return rightEncoder.getLinearVelocity();
    }

    public void zeroEncoders() {
        leftEncoder.zero();
        rightEncoder.zero();
    }

    public void off()
    {
        this.driveLeft.set(0);
        this.driveRight.set(0);
    }

    @Override
    public void stop()
    {
        off();
    }

    @Override
    protected void putNetworkTableInfo()
    {
        super.getSubsystemNetworkTable().getEntry("leftWheelSpeed").setDouble(driveLeft.get());
        super.getSubsystemNetworkTable().getEntry("rightWheelSpeed").setDouble(driveRight.get());
    }
}