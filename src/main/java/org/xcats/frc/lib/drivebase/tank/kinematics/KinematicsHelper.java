package org.xcats.frc.lib.drivebase.tank.kinematics;

import edu.wpi.first.math.kinematics.DifferentialDriveKinematics;

public class KinematicsHelper {
    private KinematicsParams params;
    private DifferentialDriveKinematics kinematics;
    public KinematicsHelper(KinematicsParams params) {
        this.params = params;
        this.kinematics = new DifferentialDriveKinematics(params.kTrackwidth);
    }

    public KinematicsParams getParams() {
        return this.params;
    }

    public DifferentialDriveKinematics getKinematics() {
        return this.kinematics;
    }
}
