package org.xcats.frc.lib.drivebase.tank.kinematics;

public class KinematicsParams {
    public final double ks; // volts
    public final double kv; // Volt Seconds Per Meter,
    public final double ka; // Volt Seconds Squared Per Meter,
    public final double kPDriveVel;
    public final double kTrackwidth; // Meters
    public final double kMaxSpeed; // Meters per Second
    public final double kMaxAcceleration; // Meters per second squared
    public final double kRamseteB;
    public final double kRamseteZeta;

    public KinematicsParams(double ks, double kv, double ka, double kPDriveVel, double kTrackwidth, double kMaxSpeed, double kMaxAcceleration, double kRamseteB, double kRamseteZeta) {
        this.ks = ks;
        this.kv = kv;
        this.ka = ka;
        this.kPDriveVel = kPDriveVel;
        this.kTrackwidth = kTrackwidth;
        this.kMaxSpeed = kMaxSpeed;
        this.kMaxAcceleration = kMaxAcceleration;
        this.kRamseteB = kRamseteB;
        this.kRamseteZeta = kRamseteZeta;
    }

    public KinematicsParams(double ks, double kv, double ka, double kPDriveVel, double kTrackwidth, double kMaxSpeed, double kMaxAcceleration) {
        this(ks, kv, ka, kPDriveVel, kTrackwidth, kMaxSpeed, kMaxAcceleration, 2, 0.7);
    }
}